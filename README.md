# AMD Drop HOWTO (EU)

## What is it?

AMD sells GPUs directly in their web shop, approximately once a week. These events are called drops. They consist of a smallish amount of stock for the following GPUs in AMD's reference design:

* Radeon RX 6700 XT
* Radeon RX 6800
* Radeon RX 6800 XT (normal or Midnight Black edition, same specs)
* Radeon RX 6900 XT

## When is it?

Over the past couple of weeks, all of the drops happened on Thursday, with the countdown usually starting around 16:00 CE(S)T (14:00-15:00 UTC). If you're lucky, a kind soul will post a notification on e.g. the PartAlert Discord.

### Can I get notified about impending drops?

Probably, yes. They usually make some internal changes to the shop in preparation for drops, and bots can pick up on these by detecting what's commonly called a CAT 16 code.

One way to get alerts is to search for `amd_cat_checker` on Telegram (or use this link: https://t.me/amd_cat_checker). These alerts will be well in advance of the actual start of the drop, though, i.e. up to 2 hours before.

In addition I run a Telegram channel that notifies you within roughly a minute of the drop countdown starting. Unfortunately I have to run it manually each week so it may not work every time. No guarantees. To use it, search for `amd_eu_queue` on Telegram, or use this link: https://t.me/amd_eu_queue.

## How does it work?

In late July, AMD introduced a queue system, i.e. anyone who wants a GPU has to wait for their turn. The queue is launched 5-15 minutes before the drop starts - if you visit the store page during this time, you will see a very different page that gives you a reCAPTCHA to solve (and possibly an additional, much simpler, CAPTCHA at a random point in time afterwards). Submit both and you get a countdown indicating when the drop will begin. Once the drop starts, the most popular models sell out within a few minutes, after about 10 minutes usually all GPUs are sold out; often less than that.

Stay on that countdown page to automatically get a *random* spot in the queue when the drop starts. If you go to the queue page after the drop has started, it's basically impossible to get a good enough spot.

When it's your turn, you get redirected to the shop where you can try buying one of the GPUs. (See below for which ones are most likely to work.) Putting a GPU in your cart does not reserve it, be quick with completing the order process! You can use PayPal or credit card. Using PayPal is faster because all of your info there, including shipping address, will be used automatically. Make sure to log into PayPal a few minutes before it's your turn in the queue to avoid any slowdowns (PayPal automatically logs you out again after five minutes, though).

If the "add to cart" buttons don't work or you don't get an order confirmation, the GPU you tried is probably already sold out, but feel free to try again just in case.

## What can I hope to get?

If you get a very early spot in the queue (just 1-2 minutes of waiting time): 6800 or 6800 XT, the most popular GPUs. There is usually fairly small stock of both, about 100 or less each for all of EU. Don't expect to get any 6800s after 2 minutes.

The next most likely GPU you can get is a 6900 XT, but those will probably be gone, too, after just a few minutes.

Finally, 6700 XT is usually best stocked and will be the final GPU to sell out.

Recently the drops have been zerg-rushed even more than before, so now *all* models will likely be gone after much less than 10 minutes.

## Can I cheat?

I don't know if there's a way to skip the queue that actually works, but there is a trick to get multiple spots in the queue. Be sure to set these up after the countdown appears, but before the drop starts!

* You can use multiple browsers.
* For each browser, you can use one normal tab and one incognito tab.
* Some browsers have add-ons that allow you to set up "containers", each of which will allow you a separate spot in the queue. You can create as many containers as you like. See the next section for concrete steps.
* Don't go overboard. For most entries into the queue you have to solve a CAPTCHA.
* If you spam the AMD servers with too many requests, your IP address will get banned (most likely until the drop is over). I wouldn't chance opening 200 queues in a very short time, for instance.
* You can verify that each browser/container is giving you a separate spot in the queue: each should display a different "queue ID".

### What's the easiest way to get many spots?

Install Firefox and the [Temporary Containers](https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/) add-on. Now you can open isolated tabs using the button in the title bar (with the alarm clock / plus icon) or by pressing Alt+C.

For more comfort, open the add-on settings (Ctrl+Shift+A -> choose add-on -> choose "Options" in three-dots menu), go to "Isolation", "Per Domain", enter "www.amd.com", set "Always open in" to "Enabled". Now any time you open www.amd.com in any tab, it will go into a separate container.

Next, go to the store and bookmark it (drag the tab title into your bookmarks toolbar; you can enable the toolbar by pressing Ctrl+Shift+B). You can now open separate sessions by middle-mouse clicking or Ctrl-clicking the bookmark.

If you prefer Chrome a whole lot, check out the add-on "Session Box". I haven't tried it myself but I know that this is what people use in Chrome. Apparently it has a limit on simultaneous sessions that you have to pay to remove... possibly there are other add-ons that don't have a limitation. Don't ask me, I don't know.

### What's the deal with the bans / white error pages?

I performed some tests and will share my findings, but please note that **the limits may change at any time**, so if in doubt, be very conservative.

* Too many hits on www.amd.com will get you IP banned for about *5-10 minutes* (meaning your IP address will be unable to access the store; you'll get a nondescript error message about the page being unavailable). There is no official information on what the limits are.
* I did some testing myself, and I could reload and open new tabs *every 2-3 seconds* for at least a minute without getting banned.

### What do I do with my queue spots?

Look at the estimated waiting time on each. Any that says more than, say, 40 minutes, is pretty much guaranteed to be worthless and you can close it immediately. In fact, close all but the best 2-3 spots, just in case one of them bugs out. Pay most attention to the one with the lowest estimated waiting time.

Good luck!
